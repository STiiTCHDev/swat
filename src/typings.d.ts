/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
    id: string;
}

/* ngx json files */
declare module '*.json' {
    const value: any;
    export default value;
}