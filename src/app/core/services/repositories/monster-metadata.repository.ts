import { Injectable } from '@angular/core';
import Datastore from 'nedb-promises';

import { DatastoresService } from '@core/services/datastores.service';

@Injectable({
    providedIn: 'root'
})
export class MonsterMetadataRepository {

    private store: Datastore;

    constructor(private stores: DatastoresService) {
        this.store = this.stores.get('monster-metadatas');
    }

    public async find(query): Promise<any> {
        return this.store.find(query)
                    .catch((error) => {
                        console.log(error);
                    });
    }

    public async findAll(): Promise<any> {
        return this.store.find({})
                    .catch((error) => {
                        console.log(error);
                    });
    }

    public async insert(metadata: MonsterMetadata | MonsterMetadata[]): Promise<any> {
        return this.store.insert(metadata)
                    .catch((error) => {
                       console.log(error);
                    });
    }

    public async count(query): Promise<any> {
        return this.store.count(query);
    }

}
