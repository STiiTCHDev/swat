import { Monster } from '@shared/models/monsters/monster.model';

export class MonstersContainer {

    private monsters: Monster[];

    constructor() {
        this.monsters = [];
    }

    public getMonsters(): Monster[] {
        return this.monsters;
    }

    public addMonster(monster: Monster): void {
        this.monsters.push(monster);
    }

    public findMonsterById(id: number): Monster {
        const monster = this.monsters.find(element => {
            return element.Id === id;
        });

        return monster;
    }

}

