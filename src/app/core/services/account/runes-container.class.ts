﻿import { Monster } from '@shared/models/monsters/monster.model';
import { Filter } from '@shared/filters/filter.interface';
import { Rune } from '@shared/models/runes/rune.model';

export interface RuneFilterPipe {
    [key: string]: Filter<Rune>;
}

export class RunesContainer {

    private runes: Rune[];

    constructor() {
        this.runes = [];
    }

    public getRunes(): Rune[] {
        return this.runes;
    }

    public addRune(rune: Rune): void {
        this.runes.push(rune);
    }

    public addMonsterRunes(monster: Monster): void {
        this.runes = this.runes.concat(monster.Runes);
    }

    public find(filters: RuneFilterPipe): Rune[] {
        let filteredRunes = this.runes;

        for (const filterType in filters) {
            if (filterType) {
                filteredRunes = filters[filterType].matchFilter(filteredRunes);
            }
        }

        console.log(filters);

        return filteredRunes;
    }
}
