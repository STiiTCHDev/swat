﻿import { Injectable } from '@angular/core';

import { RunesContainer } from '@core/services/account/runes-container.class';
import { MonstersContainer } from '@core/services/account/monsters-container.class';
import { Rune } from '@shared/models/runes/rune.model';
import { Monster } from '@shared/models/monsters/monster.model';

@Injectable()
export class AccountService {

    private accountData: AccountData;
    private runes: RunesContainer;
    private monsters: MonstersContainer;

    constructor() {
        this.runes = new RunesContainer();
        this.monsters = new MonstersContainer();
    }

    public get AccountData(): AccountData {
        return this.accountData;
    }

    public get Runes(): RunesContainer {
        return this.runes;
    }

    public get Monsters(): MonstersContainer {
        return this.monsters;
    }

    public init(filePath: string) {
        this.loadAccountData(filePath);
        this.loadRunesData();
        this.loadMonstersData();
    }

    public loadAccountData(filePath: string) {
        const file = window.fs.readFileSync(filePath, 'utf-8');
        this.accountData = JSON.parse(file);
    }

    public loadRunesData() {
        for (const runeData of this.accountData.runes) {
            this.runes.addRune(new Rune(runeData));
        }
    }

    public loadMonstersData() {
        for (const monsterData of this.accountData.unit_list) {
            const monster = new Monster(monsterData);
            this.monsters.addMonster(monster);
            this.runes.addMonsterRunes(monster);
        }
    }
}
