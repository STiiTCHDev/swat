﻿import { Injectable } from '@angular/core';

import { MonsterMetadataRepository } from '@core/services/repositories/monster-metadata.repository';
import { SwarfarmApiService } from '@core/services/swarfarm-api.service';
import { Monster } from '@shared/models/monsters/monster.model';

@Injectable()
export class AppInitProvider {

    constructor(private metadataRepo: MonsterMetadataRepository,
                private api: SwarfarmApiService) { }

    public async load(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            console.log('INIT: App-Initializer start');

            this.metadataRepo.count({})
                .then((count) => {
                    if (count !== 0) {
                        console.log('INIT: has metadata backup');
                        this.loadFromStore()
                            .then(response => {
                                resolve();
                            });
                    } else {
                        console.log('INIT: load metadata from swarfarm api');
                        this.loadFromApi()
                            .then(response => {
                                resolve();
                            });
                    }
                });
        });
    }

    private loadFromApi(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.api.getMonsters()
                .then((response) => {
                    console.log('INIT: metadata loaded from api');
                    console.log('INIT: start saving metadata');

                    this.metadataRepo.insert(response)
                        .then((metadatas) => {
                            console.log('INIT: metadata save done');
                        });

                    Monster.Metadata = response;
                    resolve();
                });
        });
    }

    private loadFromStore(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.metadataRepo.findAll()
                .then((results) => {
                    console.log('INIT: metadata loaded from datastore');
                    Monster.Metadata = results;
                    resolve();
                });
        });
    }
}
