import { Injectable } from '@angular/core';
import Datastore from 'nedb-promises';

@Injectable({
  providedIn: 'root'
})
export class DatastoresService {

    private datastores: Datastore[];

    constructor() {
        this.datastores = [];
    }

    public get(name: string): Datastore {
        if (this.datastores[name]) {
            return this.datastores[name];
        } else {
            this.create(name);
            return this.datastores[name];
        }
    }

    private create(name: string) {
        const options = {
            filename: name,
            autoload: true,
        };

        this.datastores[name] = Datastore.create(options);

        this.datastores[name].on('load', (datastore) => {
            console.log('DATASTORES: Datastore ' + name + ' loaded.');
        });
    }

}
