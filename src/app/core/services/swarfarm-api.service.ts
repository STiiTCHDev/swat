import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { expand } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SwarfarmApiService {

    private apiUrl: string;
    private monstersMetadataUrl: string;

    constructor(private http: HttpClient) {
        this.apiUrl = 'https://swarfarm.com/api/v2/';
        this.monstersMetadataUrl = this.apiUrl + 'monsters/';
    }

    public async getMonsters(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let metadata: MonsterMetadata[] = [];

            this.http.get(this.monstersMetadataUrl)
                .pipe(expand((result: SwarfarmApiResponse) => {
                    metadata = metadata.concat(result.results);
                    if (result.next) {
                        return this.http.get(result.next);
                    } else {
                        resolve(metadata);
                        return EMPTY;
                    }
                }))
                .toPromise();
        });
    }

}
