import { MonsterMetadataRepository } from './services/repositories/monster-metadata.repository';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { AccountService } from '@core/services/account/account.service';
import { DatastoresService } from '@core/services/datastores.service';
import { SwarfarmApiService } from '@core/services/swarfarm-api.service';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
    ],
    providers: [
        AccountService,
        DatastoresService,
        MonsterMetadataRepository,
        SwarfarmApiService,
    ]
})
export class CoreModule { }
