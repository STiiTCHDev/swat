import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'swat-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    public selectedLang: string;
    public langs = [
        {label: 'English', value: 'en'},
        {label: 'Français', value: 'fr'}
    ];
    private activeLang: string;

    constructor(public translate: TranslateService) {
        this.activeLang = 'fr'; // *** Default language in AppComponent ***
    }

    ngOnInit() { }

    public changeLang(lang: string) {
        this.activeLang = lang;
        this.translate.use(lang);
    }

}
