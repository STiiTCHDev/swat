import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { MonstersRoutingModule } from './monsters-routing.module';
import { MonstersListComponent } from './pages/monsters-list/monsters-list.component';
import { MonsterWidgetComponent } from './components/monster-widget/monster-widget.component';
import { MonsterViewComponent } from './pages/monster-view/monster-view.component';
import { MonsterIconComponent } from './components/monster-icon/monster-icon.component';


@NgModule({
    declarations: [
        MonstersListComponent,
        MonsterWidgetComponent,
        MonsterViewComponent,
        MonsterIconComponent
    ],
    imports: [
        MonstersRoutingModule,
        SharedModule
    ]
})
export class MonstersModule { }
