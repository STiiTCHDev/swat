import { AccountService } from '@core/services/account/account.service';
import { Monster } from '@shared/models/monsters/monster.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'swat-monster-view',
  templateUrl: './monster-view.component.html',
  styleUrls: ['./monster-view.component.scss']
})
export class MonsterViewComponent implements OnInit {

    public monster: Monster;

    constructor(private route: ActivatedRoute, private data: AccountService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.monster = this.data.Monsters.findMonsterById(parseInt(params['id'], 10));
        });
    }

}
