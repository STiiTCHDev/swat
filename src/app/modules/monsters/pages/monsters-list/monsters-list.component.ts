import { Component, OnInit } from '@angular/core';
import { Monster } from '@shared/models/monsters/monster.model';
import { AccountService } from '@core/services/account/account.service';

@Component({
  selector: 'swat-monsters-list',
  templateUrl: './monsters-list.component.html',
  styleUrls: ['./monsters-list.component.scss']
})
export class MonstersListComponent implements OnInit {

    public filteredMonsters: Monster[];
    public paginator: any;
    public displayedMonsters: Monster[];
    public displayType: string;

    constructor(public data: AccountService) { }

    ngOnInit() {
        this.filteredMonsters = this.data.Monsters.getMonsters();
        this.displayType = 'table';

        this.paginator = {
            pageIndex: 0,
            pageSize: 20,
        };
        this.onPageChange();
    }

    public onPageChange(event = null) {
        if (event !== null) {
            this.paginator.pageIndex = event;
        }

        const start = this.paginator.pageIndex * this.paginator.pageSize;
        const end = start + this.paginator.pageSize;

        this.displayedMonsters = this.filteredMonsters.slice(start, end);
    }

}
