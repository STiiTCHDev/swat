import { Component, Input } from '@angular/core';
import { Monster } from '@shared/models/monsters/monster.model';

@Component({
    selector: 'swat-monster-icon',
    templateUrl: './monster-icon.component.html',
    styleUrls: ['./monster-icon.component.scss']
})
export class MonsterIconComponent {

    @Input() monster: Monster;

    constructor() { }

    get ImgSrc(): string {
          return this.monster.ImageUrl;
    }

    getStarsCount(grade: number): any[] {
        const array = [];
        for (let i = 0; i < grade; i++) {
            array.push(i);
        }

        return array;
    }
}

