import { Component, OnInit, Input } from '@angular/core';
import { Monster } from '@shared/models/monsters/monster.model';

@Component({
  selector: 'swat-monster-widget',
  templateUrl: './monster-widget.component.html',
  styleUrls: ['./monster-widget.component.scss']
})
export class MonsterWidgetComponent implements OnInit {

    @Input() monster: Monster;

  constructor() { }

  ngOnInit() { }

}
