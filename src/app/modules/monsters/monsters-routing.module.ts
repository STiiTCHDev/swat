import { MonsterViewComponent } from './pages/monster-view/monster-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonstersListComponent } from '@modules/monsters/pages/monsters-list/monsters-list.component';


const routes: Routes = [
    {path: 'monsters-list', component: MonstersListComponent},
    {path: 'monster-view/:id', component: MonsterViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonstersRoutingModule { }
