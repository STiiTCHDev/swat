import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RuneFilterPipe } from '@core/services/account/runes-container.class';

import { SetTypeFilter } from '@shared/filters/runes/set-type-filter.class';
import { GradeFilter } from '@shared/filters/runes/grade-filter.class';
import { SlotFilter } from '@shared/filters/runes/slot-filter.class';
import { LevelFilter } from '@shared/filters/runes/level-filter.class';
import { IsEnchantedFilter } from '@shared/filters/runes/is-enchanted-filter.class';
import { IsGrindedFilter } from '@shared/filters/runes/is-grinded-filter.class';
import { MainAttrFilter } from '@shared/filters/runes/main-attr-filter.class';
import { InnateAttrFilter } from '@shared/filters/runes/innate-attr-filter.class';
import { SubAttrFilter } from '@shared/filters/runes/sub-attr-filter.class';
import { RarityFilter } from '@shared/filters/runes/rarity-filter.class';
import { IsAncientFilter } from '@shared/filters/runes/is-ancient-filter.class';

@Component({
    selector: 'swat-runes-filters',
    templateUrl: './runes-filters.component.html',
    styleUrls: ['./runes-filters.component.scss']
})
export class RunesFiltersComponent implements OnInit {

    public filters: any;
    @Output() filtersChange: EventEmitter<RuneFilterPipe>;

    constructor() {
        this.filtersChange = new EventEmitter();
    }

    ngOnInit() {
        this.filters = {
            set: new SetTypeFilter(),
            grade: new GradeFilter(),
            slot: new SlotFilter(),
            level: new LevelFilter(),
            rarity: new RarityFilter(),
            isAncient: new IsAncientFilter(),
            isEnchanted: new IsEnchantedFilter(),
            isGrinded: new IsGrindedFilter(),
            main: new MainAttrFilter(),
            innate: new InnateAttrFilter(),
            sub1: new SubAttrFilter(),
            sub2: new SubAttrFilter(),
            sub3: new SubAttrFilter(),
            sub4: new SubAttrFilter()
        };
    }

    onFiltersChange() {
        this.filtersChange.emit(this.filters);
    }

}
