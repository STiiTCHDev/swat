import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RuneFilterPipe } from '@core/services/account/runes-container.class';

export interface SortingParams {
    mode: string;
    direction: string;
}

@Component({
    selector: 'swat-runes-sorting',
    templateUrl: './runes-sorting.component.html',
    styleUrls: ['./runes-sorting.component.scss']
})
export class RunesSortingComponent implements OnInit {

    public sorting: SortingParams;
    @Output() sortingChange: EventEmitter<any>;

    public get AvailableModes(): string[] {
        return [
            'grade'
        ];
    }

    public get AvailableDirection(): string[] {
        return [
            'ascending',
            'descending'
        ];
    }

    constructor() {
        this.sorting = {
            mode: 'grade',
            direction: 'descending'
        };
        this.sortingChange = new EventEmitter();
    }

    ngOnInit() {

    }

    onSortingChange() {
        this.sortingChange.emit(this.sorting);
    }

}
