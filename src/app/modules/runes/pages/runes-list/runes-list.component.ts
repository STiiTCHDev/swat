import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { AccountService } from '@core/services/account/account.service';

import { Rune } from '@shared/models/runes/rune.model';
import { RuneAttr } from '@shared/models/runes/rune-attr.model';
import { RuneFilterPipe } from '@core/services/account/runes-container.class';
import { RarityFilter } from '@shared/filters/runes/rarity-filter.class';
import { SortingParams } from '../../components/runes-sorting/runes-sorting.component';

@Component({
    selector: 'swat-runes-list',
    templateUrl: './runes-list.component.html',
    styleUrls: ['./runes-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RunesListComponent implements OnInit {

    public panelState: string;
    public displayMode: string;
    public paginator: any;
    public filteredRunes: Rune[];
    public displayedRunes: Rune[];

    constructor(public account: AccountService) { }

    ngOnInit() {
        this.panelState = 'closed';
        this.displayMode = 'widgets';

        this.paginator = {
            pageIndex: 0,
            pageSize: 24
        };
        this.onFiltersChange({});
        this.onSortingChange({mode: 'grade', direction: 'descending'});
        this.onPageChange();
    }

    onPanelAction(action: string) {
        if (action === this.panelState) {
            this.panelState = 'closed';
        } else {
            this.panelState = action;
        }
    }

    onFiltersChange(filters: RuneFilterPipe) {
        this.filteredRunes = this.account.Runes.find(filters);
        console.log(this.filteredRunes);

        this.paginator.pageIndex = 0;
        this.onPageChange();
    }

    onSortingChange(sorting: SortingParams) {
        console.log(sorting);
        if (sorting.mode === 'grade') {
            this.filteredRunes = this.filteredRunes.sort((runeA, runeB) => {
                const runeAGrade = runeA.Grade > 10 ? runeA.Grade - 10 : runeA.Grade;
                const runeBGrade = runeB.Grade > 10 ? runeB.Grade - 10 : runeB.Grade;

                if (sorting.direction === 'ascending') {
                    if (runeAGrade < runeBGrade) {
                        return -1;
                    }
                    if (runeAGrade > runeBGrade) {
                        return 1;
                    }
                }
                if (sorting.direction === 'descending') {
                    if (runeAGrade > runeBGrade) {
                        return -1;
                    }
                    if (runeAGrade < runeBGrade) {
                        return 1;
                    }
                }
                return 0;
            });
        }

        this.onPageChange();
    }

    onPageChange(event = null) {
        if (event !== null) {
            this.paginator.pageIndex = event;
        }

        const start = this.paginator.pageIndex * this.paginator.pageSize;
        const end = start + this.paginator.pageSize;

        this.displayedRunes = this.filteredRunes.slice(start, end);
    }
}
