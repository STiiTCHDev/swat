import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { RunesRoutingModule } from './runes-routing.module';
import { RunesListComponent } from './pages/runes-list/runes-list.component';
import { RunesFiltersComponent } from './components/runes-filters/runes-filters.component';
import { RunesSortingComponent } from './components/runes-sorting/runes-sorting.component';

@NgModule({
    declarations: [
        RunesListComponent,
        RunesFiltersComponent,
        RunesSortingComponent
    ],
    imports: [
        RunesRoutingModule,
        SharedModule
    ],
})
export class RunesModule { }
