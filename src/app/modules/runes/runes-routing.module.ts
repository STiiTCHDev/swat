import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RunesListComponent } from '@modules/runes/pages/runes-list/runes-list.component';


const routes: Routes = [
    {path: 'runes-list', component: RunesListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RunesRoutingModule { }
