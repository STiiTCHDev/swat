// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

// ngx-translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { translateFactory } from '@shared/utils/translate-electron-loader';

// Shared Modules

// App
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from '@core/core.module';
import { AppInitProvider } from '@core/services/app-init.provider';
import { RunesModule } from '@modules/runes/runes.module';
import { MonstersModule } from '@modules/monsters/monsters.module';
import { SettingsModule } from '@modules/settings/settings.module';

export function AppInitProviderFactory(provider: AppInitProvider) {
    return () => provider.load();
}

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: translateFactory
            }
        }),
        CoreModule,
        RunesModule,
        MonstersModule,
        SettingsModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        AppInitProvider,
        { provide: APP_INITIALIZER, useFactory: AppInitProviderFactory, deps: [AppInitProvider], multi: true }
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
