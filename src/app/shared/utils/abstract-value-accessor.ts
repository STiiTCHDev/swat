﻿import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef } from '@angular/core';

export abstract class AbstractValueAccessor<T> implements ControlValueAccessor {

    private _value: T;

    private _onChange  = (value: T) => {};
    private _onTouched = () => {};

    get value(): T {
        return this._value;
    }

    set value(value: T) {
        if (value !== this._value) {
            this._value = value;
            this._onChange(value);
        }
    }

    writeValue(value: T) {
        this._value = value;
    }

    registerOnChange(fn: (value: T) => void) {
        this._onChange = fn;
    }

    registerOnTouched(fn: () => void) {
        this._onTouched = fn;
    }

}

export function createProvider(type: any) {
    return {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => type),
        multi: true
    };
}
