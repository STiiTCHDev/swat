import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';

import * as translationEn from '@assets/i18n/en.json';
import * as translationFr from '@assets/i18n/fr.json';

interface Translations {
    [key: string]: any;
}

const TRANSLATIONS: Translations = {
    'en': translationEn,
    'fr': translationFr
};

export class TranslateElectronLoader implements TranslateLoader {

    private translations: any;

    constructor() {
        this.translations = {};

        // this.translations['en'] = JSON.parse(window.fs.readFileSync('src/assets/i18n/en.json', 'utf-8'));
        // this.translations['fr'] = JSON.parse(window.fs.readFileSync('src/assets/i18n/fr.json', 'utf-8'));
        this.translations['en'] = TRANSLATIONS.en.default;
        this.translations['fr'] = TRANSLATIONS.fr.default;
    }

    public getTranslation(lang: string): Observable<any> {
        return of(this.translations[lang]);
    }

}

export function translateFactory() {
    return new TranslateElectronLoader();
}
