﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { RuneWidgetComponent } from '@shared/components/rune-widget/rune-widget.component';
import { RuneIconComponent } from '@shared/components/rune-icon/rune-icon.component';
import { PaginatorComponent } from './components/paginator/paginator.component';


@NgModule({
    declarations: [
        RuneWidgetComponent,
        RuneIconComponent,
        PaginatorComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        TranslateModule,

        RuneWidgetComponent,
        RuneIconComponent,
        PaginatorComponent
    ]
})
export class SharedModule { }
