﻿/**
 * Swarfarm Api Response
 */
interface SwarfarmApiResponse {
    count: number;
    next: string;
    previous: string;
    results: MonsterMetadata[];
}

