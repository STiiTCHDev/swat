
interface MonsterStats {
    hp: number;
    atk: number;
    def: number;
    spd: number;
    criticalRate: number;
    criticalDamage: number;
    resist: number;
    accuracy: number;
}
