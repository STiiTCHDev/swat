import { Rune } from '@shared/models/runes/rune.model';


/**
 * Summoners War Monster
 */
export class Monster {

    public static Metadata: MonsterMetadata[];

    private data: MonsterData;
    private runes: Rune[];

    public constructor(monsterData: MonsterData) {
        this.data = monsterData;
        this.runes = [];

        for (const rune of this.data.runes) {
            this.runes.push(new Rune(rune, this));
        }
    }

    public get Metadata(): MonsterMetadata {
        if (Monster.Metadata) {
            return Monster.Metadata.find((element) => {
                return element.com2us_id === this.data.unit_master_id;
            });
        }
    }

    public get Name(): string {
        return this.Metadata.name;
    }

    public get ImageUrl(): string {
        return 'https://swarfarm.com/static/herders/images/monsters/' + this.Metadata.image_filename;
    }

    public get Id(): number {
        return this.data.unit_id;
    }

    public get Level(): number {
        return this.data.unit_level;
    }

    public get IsAwakened(): boolean {
        if (this.Metadata.awakens_from === null) {
            return false;
        } else {
            return true;
        }
    }

    public get Grade(): number {
        return this.data.class;
    }

    public get Element(): Element {
        return Element[this.data.attribute];
    }

    public get BaseStats(): MonsterStats {
        return {
            hp: this.data.con * 15,
            atk: this.data.atk,
            def: this.data.def,
            spd: this.data.spd,
            criticalRate: this.data.critical_rate,
            criticalDamage: this.data.critical_damage,
            resist: this.data.resist,
            accuracy: this.data.accuracy,
        };
    }

    public get Runes(): Rune[] {
        return this.runes;
    }

    public setRune(rune: Rune) {
        this.runes.push(rune);
    }

}
