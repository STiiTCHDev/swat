/**
 * Monster data from SW Exporter file
 */
interface MonsterData {
    unit_id: number;
    wizard_id: number;
    island_id: number;
    pos_x: number;
    pos_y: number;
    building_id: number;
    unit_master_id: number;

    unit_level: number;
    experience: number;
    exp_gained: number;
    exp_gain_rate: number;
    class: number;
    attribute: number;
    con: number;
    atk: number;
    def: number;
    spd: number;
    resist: number;
    accuracy: number;
    critical_rate: number;
    critical_damage: number;

    skills: [[number]];
    runes: RuneData[];

    costume_master_id: number;
    trans_items: any[];
    create_time: string;
    source: number;
    homunculus: number;
    homunculus_name: string;
}
