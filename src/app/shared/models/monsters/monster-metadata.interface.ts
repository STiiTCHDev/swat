﻿/**
 * Monster Metadata from Swarfarm API
 */
interface MonsterMetadata {
    id: number;
    url: string;
    com2us_id: number;
    family_id: number;
    name: string;
    image_filename: string;
    element: string;
    archetype: string;
    base_stars: number;
    natural_stars: number;
    obtainable: boolean;
    can_awaken: true;
    awaken_level: number;
    awaken_bonus: string;
    skills: number[];
    skill_ups_to_max: number;
    leader_skill: any; // TODO
    homunculus_skills: any[]; // TODO

    base_hp: number;
    base_attack: number;
    base_defense: number;
    speed: number;
    crit_rate: number;
    crit_damage: number;
    resistance: number;
    accuracy: number;
    raw_hp: number;
    raw_attack: number;
    raw_defense: number;
    max_lvl_hp: number;
    max_lvl_attack: number;
    max_lvl_defense: number;

    awakens_from: number;
    awakens_to: number;
    awaken_mats_fire_low: number;
    awaken_mats_fire_mid: number;
    awaken_mats_fire_high: number;
    awaken_mats_water_low: number;
    awaken_mats_water_mid: number;
    awaken_mats_water_high: number;
    awaken_mats_wind_low: number;
    awaken_mats_wind_mid: number;
    awaken_mats_wind_high: number;
    awaken_mats_light_low: number;
    awaken_mats_light_mid: number;
    awaken_mats_light_high: number;
    awaken_mats_dark_low: number;
    awaken_mats_dark_mid: number;
    awaken_mats_dark_high: number;
    awaken_mats_magic_low: number;
    awaken_mats_magic_mid: number;
    awaken_mats_magic_high: number;

    source: []; // TODO
    fusion_food: boolean;
    homunculus: boolean;
    craft_cost: any; // TODO
    craft_materials: []; // TODO
}
