/**
 * Account data from SW Exporter
 */
interface AccountData {
    runes: RuneData[];
    unit_list: MonsterData[];
}
