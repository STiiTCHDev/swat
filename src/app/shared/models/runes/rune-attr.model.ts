import { Attribute } from '@shared/models/enums/attribute.enum';

/**
 * Summoners War Rune Attribute
 */
export class RuneAttr {

    private type: Attribute;
    private value: number;
    private enchanted: number;
    private grindBonus: number;

    public constructor(attrProperties: number[]) {
        this.type = attrProperties[0];
        this.value = attrProperties[1];
        this.enchanted = -1;
        this.grindBonus = -1;

        if (attrProperties[2] !== undefined) {
            this.enchanted = attrProperties[2];
        }

        if (attrProperties[3] !== undefined) {
            this.grindBonus = attrProperties[3];
        }
    }

    public get Type(): Attribute {
        return this.type;
    }

    public get TypeName(): string {
        return Attribute[this.type];
    }

    public get Value(): number {
        return this.value + (this.grindBonus > 0 ? this.grindBonus : 0);
    }

    public get Enchanted(): number {
        return this.enchanted;
    }

    public get IsEnchanted(): boolean {
        return this.enchanted === 1;
    }

    public get GrindBonus(): number {
        return this.grindBonus;
    }

    public get IsGrinded(): boolean {
        return this.grindBonus > 0;
    }

    public toString(): string {
        return this.TypeName + ' +' + this.Value;
    }

    public get ValueString(): string {
        const percentTypes = [2, 4, 6, 9, 10, 11, 12];
        const isPercent = percentTypes.includes(this.type);

        let value: string = this.Value.toString();

        if (isPercent) {
            value += '%';
        }

        if (this.grindBonus > 0) {
            value += '(' + this.value;
            if (isPercent) {
                value += '%';
            }
            value += '+' + this.grindBonus;
            if (isPercent) {
                value += '%';
            }
            value += ')';
        }

        return value;
    }

}
