import {RuneAttr} from '@shared/models/runes/rune-attr.model';
import {RuneSet} from './rune-set.enum';
import {Monster} from '@shared/models/monsters/monster.model';

/**
 * SW Rune
 */
export class Rune {

    private data: RuneData;
    private owner: Monster;

    private main: RuneAttr;
    private innate: RuneAttr;
    private subs: RuneAttr[];

    public constructor(runeData: RuneData, owner: Monster = null) {
        this.data = runeData;

        this.main = new RuneAttr(runeData.pri_eff);

        if (this.data.prefix_eff[0]) {
            this.innate = new RuneAttr(runeData.prefix_eff);
        } else { this.innate = null; }

        this.subs = [];
        for (const sub of runeData.sec_eff) {
            this.Subs.push(new RuneAttr(sub));
        }

        if (owner !== null) {
            this.owner = owner;
        }
    }

    public get Id(): number {
        return this.data.rune_id;
    }

    public get AssignedId(): number {
        return this.data.occupied_id;
    }

    public get IsAssigned(): boolean {
        return this.data.occupied_id !== 0;
    }

    public get Slot(): number {
        return this.data.slot_no;
    }

    public get SetTypeName(): string {
        return RuneSet[this.data.set_id];
    }

    public get Grade(): number {
        return this.data.class;
    }

    public get Level(): number {
        return this.data.upgrade_curr;
    }

    public get Rarity(): number {
        return this.Subs.length;
    }

    public get IsAncient(): boolean {
        if (this.Grade > 10) {
            return true;
        }

        return false;
    }

    public get BaseRarity(): number {
        return this.data.extra;
    }

    public get Owner(): Monster {
        if (this.owner !== undefined) {
            return this.owner;
        }

        return null;
    }

    public get Main(): RuneAttr {
        return this.main;
    }

    public get Innate(): RuneAttr {
        return this.innate;
    }

    public get Subs(): RuneAttr[] {
        return this.subs;
    }

    public get IsGrinded(): boolean {
        let itIs = false;
        for (const sub of this.subs) {
            if (sub.IsGrinded) {
                itIs = true;
            }
        }
        return itIs;
    }

    public get IsEnchanted(): boolean {
        let itIs = false;
        for (const sub of this.subs) {
            if (sub.IsEnchanted) {
                itIs = true;
            }
        }
        return itIs;
    }

    public hasSub(attrType: string): boolean {
        if (this.Subs) {
            for (const sub in this.Subs) {
                if (attrType === this.Subs[sub].TypeName) {
                    return true;
                }
            }
        }

        return false;
    }

    public getSubByType(attrType: string): RuneAttr {
        for (const sub in this.Subs) {
            if (attrType === this.Subs[sub].TypeName) {
                return this.Subs[sub];
            }
        }

        return null;
    }
}
