import { Component, Input, Output, EventEmitter, OnChanges, HostListener, ElementRef } from '@angular/core';

@Component({
    selector: 'swat-paginator',
    templateUrl: './paginator.component.html',
    styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnChanges {

    @Input() length: number;
    @Input() pageSize: number;
    @Output() pageChange: EventEmitter<number>;

    public pages: number[];
    public activePage: number;
    public selectorActive: boolean;

    public get displayedPages(): number[] {
        const pages = [];

        if (this.activePage === 1) {
            pages.push(this.activePage);
            pages.push(this.activePage + 1);
            pages.push(this.activePage + 2);
        } else if (this.activePage === this.getPageCount()) {
            pages.push(this.activePage - 2);
            pages.push(this.activePage - 1);
            pages.push(this.activePage);
        } else {
            pages.push(this.activePage - 1);
            pages.push(this.activePage);
            pages.push(this.activePage + 1);
        }

        return pages;
    }

    constructor(private eRef: ElementRef) {
        this.length = 0;
        this.pageSize = 0;
        this.pageChange = new EventEmitter();
        this.pages = [];
    }

    ngOnChanges() {
        const pageCount = this.getPageCount();
        this.pages = this.getPagesArray(pageCount);
        this.activePage = 1;
        this.pageChange.emit(1);
    }

    onClickPage(pageNumber: number) {
        if (pageNumber < 1 || pageNumber > this.pages.length) {
            return;
        } else {
            this.activePage = pageNumber;
            this.pageChange.emit(this.activePage);
        }
        this.selectorActive = false;
    }

    @HostListener('document:click', ['$event'])
    onClickout(event) {
        if (!this.eRef.nativeElement.contains(event.target)) {
            this.selectorActive = false;
        }
    }

    public getPageCount(): number {
        let totalPages = 0;

        if (this.length > 0 && this.pageSize > 0) {
            const pageCount = this.length / this.pageSize;
            totalPages = Math.floor(pageCount);
        }

        return totalPages;
    }

    private getPagesArray(pageCount: number): number[] {
        const pageArray: number[] = [];

        if (pageCount > 0) {
            for (let i = 1; i <= pageCount; i++) {
                pageArray.push(i);
            }
        }

        return pageArray;
    }
}
