﻿import { Component, Input } from '@angular/core';
import { Rune } from '@shared/models/runes/rune.model';

@Component({
    selector: 'swat-rune-icon',
    templateUrl: './rune-icon.component.html',
    styleUrls: ['./rune-icon.component.scss']
})
export class RuneIconComponent {

    @Input() rune: Rune;

    constructor() { }

    get BgImgSrc(): string {
        const rarityNames = ['normal', 'magic', 'rare', 'hero', 'legend'];
        return 'assets/images/runes/bg/bg_' + rarityNames[this.rune.Rarity] + '.png';
    }

    get TypeImgSrc(): string {
        if (this.rune.IsAncient) {
            return 'assets/images/runes/type/ancient_rune' + this.rune.Slot + '.png';
        }
        return 'assets/images/runes/type/rune' + this.rune.Slot + '.png';
    }

    get SetImgSrc(): string {
        return 'assets/images/runes/sets/' + this.rune.SetTypeName.toLowerCase() + '.png';
    }

    getStarsCount(grade: number): any[] {
        if (grade > 10) {
            grade = grade - 10;
        }

        const array = [];
        for (let i = 0; i < grade; i++) {
            array.push(i);
        }

        return array;
    }

}
