﻿import { Component, OnInit, Input } from '@angular/core';
import { Rune } from '@shared/models/runes/rune.model';
import { faRedo } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'swat-rune-widget',
    templateUrl: './rune-widget.component.html',
    styleUrls: ['./rune-widget.component.scss']
})
export class RuneWidgetComponent implements OnInit {

    @Input() rune: Rune;

    constructor() { }

    ngOnInit() {}

}
