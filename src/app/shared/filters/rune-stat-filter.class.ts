import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Attribute } from '@shared/models/enums/attribute.enum';
import { Rune } from '@shared/models/runes/rune.model';

export abstract class RuneStatFilter implements RuneFilter<string> {

    public attribute: string;
    public value: string;
    public mode: string;

    protected abstract noFilterAttribute: string;

    public get AvailableAttributes(): string[] {
        const attributes = [
            this.noFilterAttribute
        ];

        for (const attr in Attribute) {
            if (!Number(attr)
            && attr !== '0'
            && attr !== 'Neg'
            && attr !== 'Null'
            && attr !== 'SpeedPercent') {
                attributes.push(attr);
            }
        }

        return attributes;
    }

    public get AvailableModes(): string[] {
        return ['all', '<=', '=', '>='];
    }

    constructor (attribute: string = 'all', value: string = '0', mode: string = 'all') {
        this.attribute = attribute;
        this.value = value;
        this.mode = mode;
    }

    public abstract matchFilter(runes: Rune[]): Rune[];
}
