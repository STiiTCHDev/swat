export interface Filter<T> {
    matchFilter(collection: T[]): T[];
}
