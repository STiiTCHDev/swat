import { Rune } from '@shared/models/runes/rune.model';
import { RuneStatFilter } from '@shared/filters/rune-stat-filter.class';

export class MainAttrFilter extends RuneStatFilter {

    protected noFilterAttribute = 'all';

    constructor (attribute: string = 'all', value: string = '0', mode: string = 'all') {
        super(attribute, value, mode);
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (this.attribute === 'all') {
            return runes;
        }

        return runes.filter(rune => {
            const typeName = rune.Main.TypeName;
            if (typeName === this.attribute) {
                if (this.mode === 'all') {
                    return true;
                }
                if (this.mode === '=') {
                    return rune.Main.Value === Number(this.value);
                }
                if (this.mode === '<=') {
                    return rune.Main.Value <= Number(this.value);
                }
                if (this.mode === '>=') {
                    return rune.Main.Value >= Number(this.value);
                }
            }
        });
    }

}
