import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Rune } from '@shared/models/runes/rune.model';

export class RarityFilter implements RuneFilter<string> {
    public value: string;

    public get AvailableValues(): string[] {
        return ['all', 'normal', 'magic', 'rare', 'hero', 'legend'];
    }

    constructor(value: string = 'all') {
        this.value = value;
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (this.value === 'all') {
            return runes;
        }

        return runes.filter(rune => {
            if (rune.BaseRarity === this.AvailableValues.indexOf(this.value) - 1 ) {
                return true;
            }
        });
    }
}
