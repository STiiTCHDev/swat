import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Rune } from '@shared/models/runes/rune.model'; 

export class LevelFilter implements RuneFilter<number> {

    public value: number;
    public mode: string;

    public get AvailableValues(): number[] {
        const values = [-1, 0, 3, 6, 9, 12, 15];

        return values;
    }

    public get AvailableModes(): string[] {
        return ['<=', '=', '>='];
    }

    constructor(value: number = -1, mode: string = '=') {
        this.value = value;
        this.mode = mode;
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (Number(this.value) === -1) {
            return runes;
        }

        return runes.filter(rune => {
            if (this.mode === '=') {
                return rune.Level === Number(this.value);
            }

            if (this.mode === '<=') {
                return rune.Level <= Number(this.value);
            }

            if (this.mode === '>=') {
                return rune.Level >= Number(this.value);
            }
        });
    }
}
