import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Rune } from '@shared/models/runes/rune.model';

export class SlotFilter implements RuneFilter<number> {

    public value: number;

    public get AvailableValues(): number[] {
        return [0, 1, 2, 3, 4, 5, 6];
    }

    constructor(value: number = 0) {
        this.value = value;
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (Number(this.value) === 0) {
            return runes;
        }

        return runes.filter(rune => {
            return rune.Slot === Number(this.value);
        });
    }

}
