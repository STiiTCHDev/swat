import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Rune } from '@shared/models/runes/rune.model';
import { RuneSet } from '@shared/models/runes/rune-set.enum';

export class SetTypeFilter implements RuneFilter<string> {

    public value: string;

    public get AvailableValues(): string[] {
        const values = [
            'All'
        ];

        for (const set in RuneSet) {
            if (!Number(set)
            && set !== '0'
            && set !== 'Null'
            && set !== '__unknown9'
            && set !== '__unknown12'
            && set !== 'Broken' ) {
                values.push(set);
            }
        }

        return values;
    }

    constructor(value: string = 'All') {
        this.value = value;
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (this.value === 'All') {
            return runes;
        }

        return runes.filter(rune => {
            return rune.SetTypeName === this.value;
        });
    }

}
