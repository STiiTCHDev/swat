import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Rune } from '@shared/models/runes/rune.model';

export class IsGrindedFilter implements RuneFilter<string> {

    public value: string;

    constructor(value: string = 'all') {
        this.value = value;
    }

    public get AvailableValues(): string[] {
        return [
            'all',
            'false',
            'true'
        ];
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (this.value === 'all') {
            return runes;
        }

        return runes.filter(rune => {
            if (this.value === 'true') {
                return rune.IsGrinded === true;
            } else {
                return rune.IsGrinded === false;
            }
        });
    }
}
