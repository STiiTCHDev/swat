﻿import { Attribute } from '@shared/models/enums/attribute.enum';
import { Rune } from '@shared/models/runes/rune.model';
import { RuneStatFilter } from '@shared/filters/rune-stat-filter.class';

export class SubAttrFilter extends RuneStatFilter {

    public noFilterAttribute = 'none';

    constructor (attribute: string = 'none', value: string = '0', mode: string = 'all') {
        super(attribute, value, mode);
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (this.attribute === this.noFilterAttribute) {
            return runes;
        }

        return runes.filter(rune => {
            const sub = rune.getSubByType(this.attribute);

            if (sub) {
                if (this.mode === 'all') {
                    return true;
                }
                if (this.mode === '=') {
                    return sub.Value === Number(this.value);
                }
                if (this.mode === '<=') {
                    return sub.Value <= Number(this.value);
                }
                if (this.mode === '>=') {
                    return sub.Value >= Number(this.value);
                }
            }
        });
    }
}
