import { RuneFilter } from '@shared/filters/rune-filter.interface';
import { Rune } from '@shared/models/runes/rune.model';

export class GradeFilter implements RuneFilter<number> {

    public value: number;
    public mode: string;

    public get AvailableValues(): number[] {
        const values = [];

        for (let i = 0; i < 7; i++) {
            values.push(i);
        }

        return values;
    }

    public get AvailableModes(): string[] {
        return ['<=', '=', '>='];
    }

    constructor(value: number = 0, mode: string = '=') {
        this.value = value;
        this.mode = mode;
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (Number(this.value) === 0) {
            return runes;
        }

        return runes.filter(rune => {
            if (this.mode === '=') {
                return rune.Grade === Number(this.value);
            }

            if (this.mode === '<=') {
                return rune.Grade <= Number(this.value);
            }

            if (this.mode === '>=') {
                return rune.Grade >= Number(this.value);
            }
        });
    }
}
