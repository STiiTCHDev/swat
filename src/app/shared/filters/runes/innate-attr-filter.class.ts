﻿import { Rune } from '@shared/models/runes/rune.model';
import { RuneStatFilter } from '@shared/filters/rune-stat-filter.class';

export class InnateAttrFilter extends RuneStatFilter {

    protected noFilterAttribute = 'none';

    constructor (attribute: string = 'none', value: string = '0', mode: string = 'all') {
        super(attribute, value, mode);
    }

    public matchFilter(runes: Rune[]): Rune[] {
        if (this.attribute === this.noFilterAttribute) {
            return runes;
        }

        return runes.filter(rune => {
            const typeName = rune.Innate.TypeName;
            if (typeName === this.attribute) {
                if (this.mode === 'all') {
                    return true;
                }
                if (this.mode === '=') {
                    return rune.Innate.Value === Number(this.value);
                }
                if (this.mode === '<=') {
                    return rune.Innate.Value <= Number(this.value);
                }
                if (this.mode === '>=') {
                    return rune.Innate.Value >= Number(this.value);
                }
            }
        });
    }
}