import { Filter } from './filter.interface';
import { Rune } from '@shared/models/runes/rune.model';

export interface RuneFilter<T> extends Filter<Rune> {
    value: T;
}
