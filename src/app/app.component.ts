import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AccountService } from '@core/services/account/account.service';

@Component({
    selector: 'swat-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(private translate: TranslateService,
                private account: AccountService) { }

    ngOnInit() {
        // Init app locale
        this.translate.addLangs(['en', 'fr']);
        this.translate.setDefaultLang('fr');

        // Loading data
        this.account.init('src/assets/data/playerdata.json');
    }
}
