import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouteReuseStrategy } from '@angular/router';

import { AppRouteReuseStrategy } from '@shared/utils/app-route-reuse-strategy';


const routes: Routes = [
    {path: '', redirectTo: '/runes-list', pathMatch: 'full'}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
    exports: [ RouterModule ],
    providers: [
        {
            provide: RouteReuseStrategy,
            useClass: AppRouteReuseStrategy
        }
    ]
})
export class AppRoutingModule { }
